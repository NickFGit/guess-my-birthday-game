from random import randint

users_name = input("Hi! What is your name?\n") #getting the input for name

for guess_number in range(1,6): #loop all 5 times
    print("Guess " + str(guess_number) + " : " + users_name + " were you born in " + str(randint(0,12)) + "/" + str(randint(1924,2004)) + "?") #is this guess correct
    answer = input("yes or no? ") #take input for yes or no
    if answer == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")